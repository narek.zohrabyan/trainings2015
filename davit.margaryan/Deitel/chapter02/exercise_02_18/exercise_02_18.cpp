/// Asks the user to enter two integers
/// then prints the larger number.

#include <iostream> /// allows program to perform intput and output

/// function main begins program execution
int
main()
{
    /// initialization of variables
    int number1 = 0, number2 = 0;

    /// insert data
    std::cout << "Enter two integers." << std::endl;
    std::cout << "Number1: ";
    std::cin >> number1; /// input number1
    std::cout << "Number2: ";
    std::cin >> number2; /// input number2

    if(number1 > number2) { /// if number1 larger then number2
        std::cout << number1 << " is larger." << std::endl; /// print number1
    }
    if(number2 > number1) { /// if number2 larger then number1
        std::cout << number2 << " is larger." << std::endl; /// print number2
    }
    if(number1 == number2) { /// if number1 equal number2
        std::cout << "These numbers are equal." << std::endl; /// print info message
    }
    
    return 0; /// program ended successfully
} /// end function main
