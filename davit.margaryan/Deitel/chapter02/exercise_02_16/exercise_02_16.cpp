/// calculate the sum, subtraction
/// multiplication, quotient of two integers

#include <iostream> /// iostream lirary

/// start main function
int
main(){
    /// initalization
    int number1 = 0, number2 = 0;
 	
    /// geting data from user
    std::cout << "Enter two integers: ";
    std::cin >> number1 >> number2;
    
    /// count sum
    int sum = number1 + number2; 
    std::cout << "number1 + number2 = " << sum << std::endl;
    /// count subtraction
    int subtraction = number1 - number2; 
    std::cout << "number1 - number2 = " << subtraction << std::endl;
    /// count multiplication
    int multiplication = number1 * number2; 
    std::cout << "number1 * number2 = " << multiplication  << std::endl;   
    /// count quotient
    if(number2 != 0){ /// check if denominator is not zero
        int quotient = number1 / number2;
        std::cout << "number1 / number2 = " <<  quotient << std::endl;
    }else{ /// else
        std::cout << "The denominator can not be zero!" << std::endl; /// error message 
    }
     
    return 0;
} /// end main function
