/// The program prints chess board

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// outputong board one
    std::cout << "* * * * * * * *\n * * * * * * * *\n* * * * * * * *\n * * * * * * * *\n* * * * * * * *\n * * * * * * * *\n* * * * * * * *\n * * * * * * * *\n" << std::endl;

    /// outputong board two
    std::cout << "* * * * * * * *" << std::endl;
    std::cout << " * * * * * * * *" << std::endl;
    std::cout << "* * * * * * * *" << std::endl;
    std::cout << " * * * * * * * *" << std::endl;
    std::cout << "* * * * * * * *" << std::endl;
    std::cout << " * * * * * * * *" << std::endl;
    std::cout << "* * * * * * * *" << std::endl;
    std::cout << " * * * * * * * *" << std::endl;

    return 0; /// program ends succcessfully
} /// end function maiin
