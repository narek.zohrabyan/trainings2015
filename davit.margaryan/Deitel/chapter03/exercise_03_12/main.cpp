/// main.cpp
/// testing of Account class

#include "Account.hpp" /// includes the Account class definition
#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// creating Account objects
    Account account1(10); 
    Account account2(-10); 
    /// print balance
    std::cout << "Balance one: " << account1.getBalance() << std::endl; 
    std::cout << "Balance two: " << account2.getBalance() << std::endl;
    /// withdrowing money
    account1.debit(3);
    account2.debit(15); /// withdrowing money greather then balancei
    /// print balance
    std::cout << "Balance one: " << account1.getBalance() << std::endl; 
    std::cout << "Balance two: " << account2.getBalance() << std::endl; 
    /// adding money
    account1.debit(10); 
    account2.credit(30);
    /// print balance 
    std::cout << "Balance one: " << account1.getBalance() << std::endl; 
    std::cout << "Balance two: " << account2.getBalance() << std::endl; 

    return 0; /// program ends successfully
} /// end function main
