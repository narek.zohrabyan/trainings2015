/// The program calculates matematic constatnt e

#include <iostream> /// allows program to perform input output

/// function main begins program execution
int
main()
{
    double x = 0;
    /// prompt user to enter degree
    std::cout << "Enter x: ";
    std::cin >> x;

    unsigned long accuracy = 0;
    /// prompt user to input accuracy
    std::cout << "Enter accuracy (0 - 32): ";
    std::cin >> accuracy;
    /// error hendleing
    if (accuracy > 32) {
        std::cerr << "Error 1: The accuracy must be in range [0, 32]!\n";
        return 1;
    }

    unsigned long i = 1, factorial = 1;
    double exponental = 1.0, degree = 1;
    while (i <= accuracy) {
        factorial *= i;
        degree *= x;
        exponental += degree / factorial;
        ++i;
    }
    std::cout << "\ne^" << x << " = " << exponental << std::endl;
    return 0;
}
