/// The program calculates the factorial

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    unsigned long accuracy;
    std::cout << "Enter accuracy (0 - 32): ";
    std::cin >> accuracy;
    if (accuracy > 33) {
        std::cerr << "Error 1: The accuracy must be in range [0, 32].\n";
        return 1;
    }
    unsigned long i = 2, factorial = 1;
    while (i <= accuracy) {
        factorial *= i;
        ++i;
    }
    std::cout << accuracy << "! = " << factorial << std::endl;
    return 0;
} /// end function main
