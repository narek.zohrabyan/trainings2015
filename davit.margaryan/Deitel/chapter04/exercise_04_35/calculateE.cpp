/// The program calculates matematic constatnt e

#include <iostream> /// allows program to perform input output

/// function main begins program execution
int
main()
{
    unsigned long accuracy = 0;
    /// prompt user to input accuracy
    std::cout << "Enter accuracy (0 - 32): ";
    std::cin >> accuracy;
    /// error hendleing
    if (accuracy > 32) {
        std::cerr << "Error 1: The accuracy must be in range [0, 32]!\n";
        return 1;
    }

    double euler = 1.0;
    unsigned long factorial = 1, i = 1;
    while (i <= accuracy) {
        factorial *= i;
        euler += 1.0 / factorial;
        ++i;
    }
    std::cout << "euler = " << euler << std::endl;
    return 0;
}
