///

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program enumberecution
int
main()
{
    int number = 0;
    std::cout << "Enter number with 4 characters: ";
    std::cin >> number;
    if (number < 1000) { 
        std::cerr << "Error 1: Number must be betwen 1000 - 9999\n";
        return 1;
    } else if (number > 9999) {
        std::cerr << "Error 1: Number must be betwen 1000 - 9999\n";
        return 1;    
    }
    
    /// encode part
    int digit1 = (number / 1000 + 7) % 10;
    int digit2 = (number / 100 + 7) % 10;
    int digit3 = (number / 10 + 7) % 10;
    int digit4 = (number + 7) % 10;
    int encode = digit3 * 1000;
    encode += digit4 * 100;
    encode += digit1 * 10;
    encode += digit2;
   
    /// decode part
    int encodeDigit1 = (3 + encode / 1000) % 10;
    int encodeDigit2 = (3 + encode / 100) % 10;
    int encodeDigit3 = (3 + encode / 10) % 10;
    int encodeDigit4 = (3 + encode) % 10;
    int decode = encodeDigit3 * 1000;
    decode += encodeDigit4 * 100;
    decode += encodeDigit1 * 10;
    decode += encodeDigit2;
    
    std::cout << "\n" << std::setw(4) << number << " => "
	      << std::setw(4) << std::setfill('0')
	      << encode << " => " << decode << std::endl;
    return 0; /// program ends successfully
} /// end function main
