/// The program gates sale and calculates salary of employee

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    double sale = 0;
    
    /// prompt user to input sale
    std::cout << "Enter sales in dollars (-1 to end): ";
    std::cin >> sale; /// input sale

    /// while sale is not equal to -1
    while (sale != -1) {
        if (sale < 0) { /// if sale is valid
            std::cout << "Error: Sale can not be negative!\n";
            return 1; /// program ends with error
        }/// end if

        /// calculate and print salary
        double salary = 200 + 0.09 * sale;
        std::cout << "Salary is: " << std::fixed 
              << salary << std::endl;

        /// prompt user to input sale
        std::cout << "Enter sales in dollars (-1 to end): ";
        std::cin >> sale; /// input sale
    } /// end while

    return 0; /// program ends successfully
} /// end function main
