#include <iostream>
#include <string>

int
main()
{
    std::string line;
    std::cout << "The Twelve days of Christmas" << std::endl;
    for (int quartet = 1; quartet <= 12; ++quartet) {
        std::string day;
        switch (quartet) {
        case  1:  day = "first";    break;
        case  2:  day = "second";   break;
        case  3:  day = "third";    break;
        case  4:  day = "fourth";   break;
        case  5:  day = "fifth";    break;
        case  6:  day = "sixth";    break;
        case  7:  day = "seventh";  break;
        case  8:  day = "eighth";   break;
        case  9:  day = "ninth";    break;
        case 10:  day = "tenth";    break;
        case 11:  day = "eleventh"; break;
        case 12:  day = "twelfth";  break;
        }

        switch (quartet) {
        case  1:  line = "A Partridge in a Pear Tree\n";    break;
        case  2:  line = "Two Turtle Doves\nand "   + line; break;
        case  3:  line = "Three French Hens\n"      + line; break;
        case  4:  line = "Four Calling Birds*\n"    + line; break;
        case  5:  line = "Five Golden Rings\n"      + line; break;
        case  6:  line = "Six Geese a Laying\n"     + line; break;
        case  7:  line = "Seven Swans a Swimming\n" + line; break;
        case  8:  line = "Eight Maids a Milking\n"  + line; break;
        case  9:  line = "Nine Ladies Dancing\n"    + line; break;
        case 10: line = "Ten Lords a Leaping\n"     + line; break;
        case 11: line = "Eleven Pipers Piping\n"    + line; break;
        case 12: line = "12 Drummers Drumming\n"    + line; break;
        }

        std::cout << "\nOn the " << day << " day of Christmas\n"
    	          << "my true love sent to me:\n" << line << std::endl;
    }
    return 0;
}
