///

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    int productNumber = 0;
    
    std::cout << "Enter product number (1 - 5): ";
    std::cin >> productNumber;

    if (productNumber < 1 || productNumber > 5) {
        std::cerr << "Error 1: This product does not exists!\n";
        return 1;
    }

    int quantity = 0;
    std::cout << "Enter quantity sold per day: ";
    std::cin >> quantity;
    
    if (quantity < 0) {
        std::cerr << "Error 2: Quantity can not be negatve.\n";
        return 2;
    }

    double result = 0.0;
    switch (productNumber) {
    case 1:
        result = 7 * 2.98 * quantity;
        break;
    case 2:
        result = 7 * 2.98 * quantity;
        break;
    case 3:
        result = 7 * 2.98 * quantity;
        break;
    case 4:
        result = 7 * 2.98 * quantity;
        break;
    case 5:
        result = 7 * 2.98 * quantity;
        break;       
    }
    std::cout << "Total money: " << result << "$" << std::endl;
    return 0; /// program ends successfully
} /// end function main
