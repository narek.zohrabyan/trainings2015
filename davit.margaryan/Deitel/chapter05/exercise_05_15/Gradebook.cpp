#include "Gradebook.hpp" /// definition of Gradebook class
#include <iostream> /// input and output
#include <cstdio>
/// constructor initializes data members
Gradebook::Gradebook(std::string courseName)
{
    setCourseName(courseName); /// store course name
    aCount_ = 0; /// initialization number of grades A
    bCount_ = 0; /// initialization number of grades B
    cCount_ = 0; /// initialization number of grades C
    dCount_ = 0; /// initialization number of grades D
    fCount_ = 0; /// initialization number of grades F
    total_ = 0.0; /// total grades
} /// end constructor

/// stores course name
void
Gradebook::setCourseName(std::string courseName)
{
    if (courseName.length() <= 25) {
        courseName_ = courseName;
    } else {
        courseName_ = courseName.substr(0, 25);
        std::cout << "Name \"" << courseName << "\" exceeds maximum length(25).\n"
                  << "Limiting courseName to first 25 characters.\n" << std::endl;
    } /// end if ... else
} /// enf function setCourseName

/// returns courseName_
std::string
Gradebook::getCourseName()
{
    return courseName_;
} /// end function getCourseName

/// displays message
void
Gradebook::displayMessage()
{
    std::cout << "Welcome to the grade book for" 
              << getCourseName() << "!\n" << std::endl;
} /// end function displayMessage

/// prompt user to input grades
void
Gradebook::inputGrades()
{
    int grade;
    std::cout << "Enter the letter grades." << std::endl
              << "Enter the EOF character to end input." << std::endl;
    while ((grade = std::cin.get()) != EOF) {
        switch (grade) {
        case 'A':
        case 'a':
            ++aCount_;
            total_ += 4;
            break;
        case 'B':
        case 'b':
            ++bCount_;
            total_ += 3;
            break;
        case 'C':
        case 'c':
            ++cCount_;
            total_ += 2;
            break;
        case 'D':
        case 'd':
            ++dCount_;
            total_ += 1;
            break;
        case 'F':
        case 'f':
            ++fCount_;
            break;

        case ' ':
        case '\n':
        case '\t':
            break;

        default:
            std::cout << "Incorrect letter grade entered."
                      << "Enter a new grade." << std::endl;
            break;
        } /// end of switch
    } /// end while
} /// end function inputGrades

/// display the grade report
void
Gradebook::displayGradeReport()
{
    std::cout << "\n\nNumber of who recived each letter grade:"
              << "\nA: " << aCount_
              << "\nB: " << bCount_
              << "\nC: " << cCount_
              << "\nD: " << dCount_
              << "\nF: " << fCount_
              << "\nAverage: " << getAverage()
              << std::endl;
} /// end function displayGradeReport

/// calculate average of grades
double
Gradebook::getAverage()
{
    return total_ / (aCount_ + bCount_ + cCount_ + dCount_ + fCount_);
} /// end function getAverage