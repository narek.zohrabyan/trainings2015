#ifndef __GRADEBOOK_HPP__
#define __GRADEBOOK_HPP__

#include <string>

class Gradebook
{
public:
    Gradebook(std::string courseName);
    void setCourseName(std::string courseName);
    std::string getCourseName();
    void displayMessage();
    void inputGrades();
    void displayGradeReport();
    double getAverage();
private:
    std::string courseName_;
    int aCount_; /// number of grades A
    int bCount_; /// number of grades B
    int cCount_; /// number of grades C
    int dCount_; /// number of grades D
    int fCount_; /// number of grades F
    double total_;
};
#endif /// __GRADEBOOK_HPP__