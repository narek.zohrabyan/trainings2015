///
#include "Gradebook.hpp" /// definition of Gradebook class
#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    Gradebook myGradebook("C++ programing");
    myGradebook.displayMessage();
    myGradebook.inputGrades();
    myGradebook.displayGradeReport();
    return 0; /// program ends successfully
} /// end function main
