#include <iostream>

int
main()
{
    int type = 0;
    while (type != -1) {
        std::cout << "\nEnter employee type: \n"
            << " 1 - manager\n"
            << " 2 - hourly worker\n"
            << " 3 - comission worker\n"
            << " 4 - piece worker\n"
            << "-1 - exit" << std::endl;
        std::cout << "Type: ";
        std::cin >> type;
        double salary = 0;
        switch(type) {
        case -1: 
            return 0;
        case 1: {
                std::cout << "Enter manager salary: ";
                std::cin >> salary;
                if (salary <= 0) {
                    std::cerr << "Error 1: Manager salary can not be negative or equal zero!\n";
                    return 1;
                }
            }
            break;
        case 2: {
                double workerSalary = 0;
                std::cout << "Enter hourly worker salary: ";
                std::cin >> workerSalary;
                if (workerSalary <= 0) {
                    std::cerr << "Error 2: Hourly worker salary can not be negative or equal zero!\n";
                    return 2;
                }

                double hours = 0;
                std::cout << "Enter hours: ";
                std::cin >> hours;
                if (hours < 0) {
                    std::cerr << "Error 3: Hours can not be negatie!\n";
                    return 3;
                }

                salary = hours * workerSalary;
                if (hours > 40) {
                    salary += (40 - hours) * workerSalary / 2;
                }
            }
            break;
        case 3: {
                int grossSales = 0;
                std::cout << "Enter gross sales: ";
                std::cin >> grossSales;
                if (grossSales < 0) {
                    std::cerr << "Error 4: Comission employee gross weekly sales can not be negative!\n";
                    return 4;
                }
                salary = 250 + 0.057 * grossSales;
            }
            break;
        case 4: {
                std::cout << "Enter piece worker salary: ";
                std::cin >> salary;

                if (salary < 0) {
                    std::cerr << "Error 5: Invalid comission employee salary!\n";
                    return 5;
                }

                int amounth = 0;
                std::cout << "Enter amounth: ";
                std::cin >> amounth;
 
                if (amounth < 0) {
                    std::cerr << "Error 6: Invalid amounth!\n";
                    return 6;
                }
                salary *= amounth;
            }
            break;
        default:
            std::cerr << "Error 7: Invalid choice!\n";
            return 7;
            break;
    	}
        std::cout << "Salary: " << salary << "$" << std::endl;
    }
    return 0;
}
