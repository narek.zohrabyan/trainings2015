/// The program calculats number

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// initialization of data
    int count = 0, sum = 0;
    /// prompt user to enter number
    std::cout << "Enter count: ";
    std::cin >> count;
    for (int i = 1; i <= count; ++i) {
        int number = 0;
        std::cout << "Enter number: ";
        std::cin >> number;
        sum += number;
    }
    std::cout << "Sum is: " << sum << std::endl;

    return 0;
} /// end function main
