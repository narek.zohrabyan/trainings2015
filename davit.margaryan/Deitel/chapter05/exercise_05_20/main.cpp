///

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    for (int leg1 = 3; leg1 <= 498; ++leg1) {
        int sqrLeg1 = leg1 * leg1;
        for (int leg2 = leg1 + 1; leg2 <= 499; ++leg2) {
            int sqrLeg2 = leg2 * leg2;
            for (int hypotenus = leg2 + 1; hypotenus <= 500 
                && hypotenus < leg1 + leg2; 
                ++hypotenus) {
                int sqrtHypotenus = hypotenus * hypotenus;
                if (sqrLeg1 + sqrLeg2 == sqrtHypotenus) {
                    std::cout << hypotenus << ' ' 
                              << leg1 << ' ' 
                              << leg2 << std::endl;
                    break;
                }
            }
        }
    }
    std::cout << std::endl;
    return 0; /// program ends successfully
} /// end function main
