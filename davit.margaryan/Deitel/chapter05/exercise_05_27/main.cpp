#include <iostream> /// allows program to perform input and output

/// function main begins progrtam execution
int
main()
{
    for (int count = 1; count <= 10; ++count) {
    	if (count != 5) {
            std::cout << count << ' ';
    	}
    }
    std::cout << "\nTo skip priniting 5 without using continue." << std::endl;
    return 0; /// program ends successfully
} /// end function main