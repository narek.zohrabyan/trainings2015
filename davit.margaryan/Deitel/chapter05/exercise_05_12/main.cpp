///

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// first one
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < i; ++j) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }

    /// second one
    for (int i = 0; i < 10; ++i) {
        for (int j = i; j < 10; ++j) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }

    /// third one
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < i; ++j) {
            std::cout << ' ';
        }
        for (int k = i; k < 10; ++k) {
            std::cout << '*';
        }
        std::cout << std::endl;
    }

    /// fourth one
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 9 - i; ++j) {
            std::cout << ' ';
        }
        for (int k = 9; k < 10 + i; ++k) {
            std::cout << '*';
        }
        std::cout << std::endl;
    }


    /// complite
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < i; ++j) {
            std::cout << "*";
        }
        for (int k = i; k < 10; ++k) {
            std::cout << " ";
        }
        std::cout << " ";
        
        for (int j = i; j < 10; ++j) {
            std::cout << "*";
        }
        for (int k = 10 - i; k < 10; ++k) {
            std::cout << " ";
        }
        std::cout << " ";

        for (int j = 0; j < i; ++j) {
            std::cout << ' ';
        }
        for (int k = i; k < 10; ++k) {
            std::cout << '*';
        }
        std::cout << " ";

        for (int j = 0; j < 10 - i; ++j) {
            std::cout << ' ';
        }
        for (int k = 9; k < 10 + i; ++k) {
            std::cout << '*';
        }
        std::cout << std::endl;
    }
    return 0; /// program ends successfully
} /// end function main
