/// CALCULATE PI

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allos program to perform input and output manipuatlation

/// function main begins program execution
int
main()
{
    char sign = 1;
    int count = 0, divider = 1, testPi = 314159;
    double pi = 0.0;
    for (int divFactor = 1000, mulFactor = 100; divFactor > 0; divFactor /= 10, mulFactor *= 10) {
        int currentTestPi = testPi / divFactor;
        double delta = 0;
        do {
            pi += 4.0 / divider * sign;
            divider += 2;
            sign *= -1;
            ++count;
            delta = pi * mulFactor - currentTestPi;
        } while (delta >= 0.05 || delta < 0);
        std::cout << std::left << std::setfill(' ') << std::setw(7) << pi << " - " << count << std::endl;
    }
    return 0; /// program ends successfully
} /// end function main
