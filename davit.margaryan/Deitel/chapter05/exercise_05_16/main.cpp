///

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// include input output manipulatior

/// function main begins program execution
int
main()
{
    int amount = 100000;
    std::cout << "Year" << std::setw(22) << "Amount on deposit" << std::endl;
    for (int year = 1; year <= 10; ++year) {
        amount = (amount * 105) / 100;
        std::cout << std::setw(4) << year << std::setw(19) 
                  << amount / 100 << "." 
                  << amount % 100
                  << std::endl;
    }
    return 0; /// program ends successfully
} /// end function main
