/// calculate rate

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// include input output manipulatior

/// function main begins program execution
int
main()
{
    for (double rate = 0.05; rate < 0.11; rate += 0.01) {
        double amount = 1000.0, coefficent = 1.0 + rate;
        std::cout << "Year" << std::setw(38)
		  << "Amount on deposit rate - " 
		  << static_cast<int>(rate * 100) << '%' << std::endl;
        std::cout << std::fixed << std::setprecision(2);
        for (int year = 1; year <= 10; ++year) {
            amount *= coefficent;
            std::cout << std::setw(4) << year << std::setw(20)
                      << amount << std::endl;
        }
        std::cout << std::endl;
    }   
    return 0; /// program ends successfully
} /// end function main
