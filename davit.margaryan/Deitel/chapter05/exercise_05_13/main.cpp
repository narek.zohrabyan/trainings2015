///

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    for (int itr = 0; itr < 5; ++itr) {
        int number = 0;
        std::cout << "Enter number: ";
        std::cin >> number;
        if (number < 1 || number > 30) {
            std::cout << "Error 1: The number is out of range!\n";
            return 1;
        }
        for (int row = 0; row < number; ++row) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    return 0; /// program ends successfully
} /// end function main
