/// calculate factorial

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    int factorial = 1;
    for (int i = 1; i <= 5; ++i, factorial *= i) {
        std::cout << i << "! = " << factorial << std::endl;
    }
    return 0; /// program ends successfully
} /// end function main
