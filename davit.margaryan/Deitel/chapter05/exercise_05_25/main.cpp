/// remove break from loop
#include <iostream>
/// function main begins program execution
int
main()
{
    int count;
    for (count = 0; count < 5; ++count) {
    	std::cout << count << ' ';
    } /// end for
    std::cout << "\nBroke out of loop at count = " << count << std::endl;
    return 0; /// program ends successfully
} /// end function main