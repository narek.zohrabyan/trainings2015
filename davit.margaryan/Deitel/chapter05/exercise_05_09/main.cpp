/// The program calculates product of odd integers from 1 - 15

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    int product = 0;
    for (int i = 1; i <= 15; i += 2) {
        product += i;
    }
    std::cout << "Product: " << product << std::endl;
    return 0; /// program ends successfully
} /// end function main