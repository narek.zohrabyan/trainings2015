#include <iostream>
int
main()
{
    int n = 0;
    std::cout << "Enter odd number (1 - 19): ";
    std::cin >> n;
    if (n < 1 || n > 19) {
        std::cerr << "Error 1: The number is out of range!\n";
        return 1;
    }
    if ((n & 1) == 0) {
        std::cerr << "Error 2: The number can not be even!\n";
        return 2;
    }
    int range = n / 2;
    for (int i = 0; i < n; ++i) {
        int spaces = (range - i < 0) ? i - range : range - i;
        for (int j = 1; j <= n - spaces; ++j) {
            if (j <= spaces) {
                std::cout << " ";
            } else {
                std::cout << "*";
            } 
        }
        std::cout << std::endl;
    }
    return 0;
}
