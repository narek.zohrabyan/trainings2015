#include <iostream>
int
main()
{
    int n = 9;
    int range = n / 2;
    for (int i = 0; i <= n - 1; ++i) {
        int spaces = (range - i < 0) ? i - range : range - i;
        for (int j = 1; j <= n - spaces; ++j) {
           if (spaces >= j) {
                std::cout << " ";
            } else {
                std::cout << "*";
            }
        }
        std::cout << std::endl;
    }
    return 0;
}
