/// 

#include <iostream>

/// function main begons program execution
int
main()
{
    /// initializatin of data
    int number = 0, sum = 0, count = -1;
    /// prompt user to enter number
    while (number != 9999) { /// while number is not equal 9999
        sum += number;
        std::cout << "Enter number (9999 to end): ";
        std::cin >> number;
        ++count;
    } /// end while
    /// print averge
    std::cout << "Average: " << static_cast<double>(sum) / count << std::endl;
    return 0; /// program ends successfully
} /// end function main
