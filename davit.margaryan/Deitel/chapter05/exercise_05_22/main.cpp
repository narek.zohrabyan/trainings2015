#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    {
        int x = 0, y = 0;
        std::cout << "Enter x, y: ";
        std::cin >> x >> y;
        if ((!(x < 5) && !(y >= 7)) == !(x < 5 || y >= 7)) {
            std::cout << "(!(" << x << " < 5) && !(" << y << " >= 7))"
                      << " and !(" << x << " < 5 || " << y << " >= 7) "
                      << "are equivalent.\n";
        } else {
            std::cout << "!(" << x << " < 5) && !(" << y << " >= 7)"
                      << " and !(" << x << " < 5 || " << y << " >= 7) "
                      << "are not equivalent.\n";
        }
    }

    {
        int a = 0, b = 0, g = 0;
        std::cout << "Enter a, b, g: ";
        std::cin >> a >> b >> g;
        if ((!(a == b) || !(g != 5)) == !(a == b && g != 5)) {
            std::cout << "!(" << a << " == " << b << ")"
                      << " || !(" << g << " != 5)" 
                      << " and !(" << a << " == " << b 
                      << " && " << g << " != 5) "
                      << "are equivalent.\n";
        } else {
            std::cout << "!(" << a << " == " << b << ")"
                      << " || !(" << g << " != 5)" 
                      << " and !(" << a << " == " << b 
                      << " && " << g << " != 5) "
                      << "are not equivalent.\n";
        }
    }

    {
        int x = 0; int y = 0;
        std::cout << "Enter x, y: ";
        std::cin >> x >> y;
        if (!(x <= 8 && y > 4) == (x > 8 || (y <= 4))) {
            std::cout << "!( " << x << " <= 8 && " << y << " > 4)"
                      << " and (" << x << " > 8 || " << y << " <= 4)"
                      << " are equivalent.\n";
        } else {
            std::cout << "!( " << x << " <= 8 && " << y << " > 4)"
                      << " and (" << x << " > 8 || " << y << " < 4)"
                      << " are not equivalent.\n";
        }
    }
    
    {
        int i = 0, j = 0;
        std::cout << "Enter i, j: ";
        std::cin >> i >> j;
        if (!(i > 4 || j <= 6) == (i <= 4 && j > 6)) {
            std::cout << "!(" << i << " > 4 || " << j << " <= 6)"
                      << " and (" << i << " <= 4 && " << j << " > 6)"
                      << " are equivalent.\n";
        } else {
            std::cout << "!(" << i << " > 4 || " << j << " <= 6)"
                      << " and (" << i << " <= 4 && " << j << " > 6)"
                      << " are not equivalent.\n";
        }
    }
    return 0; /// program ends successfully
} /// end function main
