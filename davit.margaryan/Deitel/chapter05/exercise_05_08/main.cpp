///

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// initialization of count
    int count = 0;
    /// prompt user to input count of numbers
    std::cout << "Enter count of numbers: ";
    std::cin >> count; /// input number
    if (count <= 0) { /// check if count is negative
        std::cerr << "Error 1: Count must be greater then zero!\n";
        return 1; /// program ends with error
    } /// end if
    /// initialization of index and min integer
    int index = 0, min = 2147483647;
    while (++index <= count) {
        int number = 0;
        std::cout << "Enter number: ";
        std::cin >> number;
        if (number < min) {
            min = number;
        }
    }
    std::cout << "Minimum number: " << min << std::endl;
    return 0; /// program ends successfully
} /// end function main
