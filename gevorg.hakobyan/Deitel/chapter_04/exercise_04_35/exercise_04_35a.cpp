#include <iostream>

int
main()
{
    int number;

    std::cout << "Enter the none-negative number: ";
    std::cin  >> number;

    if (number < 0){
        std::cout << "Error 1: The number can't be negative.\nTry again." << std::endl;
        return 1;
    }

    if (0 == number){
        std::cout << "factorial(" << number << ") = " << 1 << std::endl;
        return 0;
    }    

    int current = 1, factorial = 1;
    while (current <= number){
        factorial *= current;
        ++current;
    }
    
    std::cout << "factorial(" << number << ") = " << factorial << std::endl;

    return 0;  
}
