#include <iostream>
#include <iomanip>

int
main()
{
    double distance, distanceSum = 0, consumptionOfGasolineSum = 0;
    
    std::cout << "Enter distance (non-positive number to quit): ";
    std::cin  >> distance;   

    while (distance > 0){
        double consumptionOfGasoline;
        std::cout << "Enter consumption of gasoline: ";
        std::cin  >> consumptionOfGasoline;
        if (consumptionOfGasoline <= 0){
            std::cout << "Error 1: Gasoline consumption can't be non-positive number.\nTry again." << std::endl;
            return 1; 
        }
        double mileGallon = distance / consumptionOfGasoline;
        std::cout << "Mile/gallon for this refueling: " << std::setprecision(2) << std::fixed << mileGallon;
        distanceSum += distance;
        consumptionOfGasolineSum += consumptionOfGasoline;
        double sumMileGallon = distanceSum / consumptionOfGasolineSum;
        std::cout << "\nSummary of values mile/gallon: " << std::setprecision(2) << std::fixed << sumMileGallon; 
        
        std::cout << "\n\nEnter distance (non-positive number to quit): ";
        std::cin  >> distance;     
    }
             
    return 0;
}
