#include <iostream>

int
main()
{
    int amount, sum = 0;

    std::cout << "Enter the amount: ";
    std::cin >> amount;
    
    for (int i = 1; i <= amount; ++i){
        int number;
    
        std::cout << "Enter the number: ";        
        std::cin  >> number;

        sum += number;
    }

    std::cout << "Sum = " << sum << std::endl;

    return 0;
}
