#include <iostream>

int
main()
{
    int x, y;

    std::cout << "x y: ";
    std::cin  >> x >> y;

    bool right1 = !(x < 5) && !(y >= 7);
    bool right2 = !((x < 5) || (y >= 7)); 

    if (right1 == right2) { 
        std::cout << "For " << x << ", " << y << " !(x < 5) && !(y >= 7) and !((x < 5) || (y >= 7)) are equivalent." << std::endl;
    } else {
        std::cout << "!(x < 5) && !(y >= 7) and !((x < 5) || (y >= 7)) are not equivalent." << std::endl;
    } 

    int a, b, g;

    std::cout << "a b g: ";
    std::cin  >> a >> b >> g;

    right1 = !(a == b) || !(g != 5);
    right2 = !((a == b) && (g != 5));

    if (right1 == right2) { 
        std::cout << "For " << a << ", " << b << ", " << g << "!(a == b) || !(g != 5) and !((a == b) && (g != 5))) are equivalent." << std::endl;
    } else {
        std::cout << "!(a == b) || !(g != 5) and !((a == b) && (g != 5))) are not equivalent." << std::endl;
    }
    
    {
        int x, y;
    
        std::cout << "x y: ";
        std::cin  >> x >> y;

        right1 = !((x <= 8) && (y > 4));
        right2 = !(x <= 8) || !(y > 4);

        if (right1 == right2) {
            std::cout << "For " << x << ", " << y << "!((x <= 8) && (y > 4)) and !(x <= 8) || !(y > 4) are equivalent." << std::endl;
        } else {
            std::cout << "!((x <= 8) && (y > 4)) and !(x <= 8) || !(y > 4) are not equivalent." << std::endl;
        }
    }

    int i, j;

    std::cout << "i j: ";
    std::cin  >> i >> j;

    right1 = !((i > 4) || (j <= 6));
    right2 = !(i > 4) && !(j <= 6);

    if (right1 == right2) {
        std::cout << "For " << i << ", " << j << "!((i > 4) || (j <= 6)) and !(i > 4) && !(j <= 6) are equivalent." << std::endl;
    } else {
        std::cout << "!((i > 4) || (j <= 6)) and !(i > 4) && !(j <= 6) are not equivalent." << std::endl;
    }

    return 0;
}
