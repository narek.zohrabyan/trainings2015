#include <iostream>
#include <iomanip>
#include <cmath>

int
main()
{
    std::cout << "Percent" << std::setw(24) << "Amount on deposit" << std::endl;
    for (int rate = 5; rate <= 10; ++rate) {
        std::cout << "For " << rate << "%" << std::endl;
        double principal = 24.00;
        for (int year = 1626; year <= 2016; ++year) {     
            principal += principal * rate / 100;
            std::cout << "In " << year << " year is " << std::setprecision(7) << principal << "$" << std::endl;
        }
        std::cout << std::endl;
    }
    return 0;
}
