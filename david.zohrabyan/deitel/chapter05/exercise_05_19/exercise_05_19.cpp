#include <iostream>
#include <iomanip>

int
main()
{
    int goldenPi = 314159, denominator = 1, termOfSeries = 0;
    double Pi = 0.0;
    int sign = 1;
    for(int divider = 1000, multiplier = 100; divider >= 1; divider /= 10, multiplier *= 10) {
        int currentGoldenPi = goldenPi / divider;
        do{
            Pi += 4.0 / denominator * sign;
            sign *= -1;
            denominator += 2;
            ++termOfSeries;
        } while (Pi * multiplier - currentGoldenPi < 0 || Pi * multiplier - currentGoldenPi >= 0.1);
           
        std::cout << termOfSeries << std::setw(10) << Pi << std::endl;
    }    
    
    return 0;
}
