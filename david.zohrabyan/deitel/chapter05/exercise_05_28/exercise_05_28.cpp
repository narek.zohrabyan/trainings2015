#include <iostream>
#include <string>

int
main()
{
    std::string lastLine;
    for(int counter = 1; counter <= 12; ++counter) {
        std::string day, middleLine;
        switch (counter) {
            case 1:  day =  "first";     middleLine = "A Partridge in a Pear\n";  break;
            case 2:  day =  "second";    middleLine = "Two Turtle Doves\n"; 
                     lastLine = "and Partridge in a Pear\n";                      break;
            case 3:  day =  "third";     middleLine = "Three French Hens\n";      break;
            case 4:  day =  "fourth";    middleLine = "Four Calling Birds\n";     break;
            case 5:  day =  "fifth";     middleLine = "Five Golden Rings\n";      break;
            case 6:  day =  "sixth";     middleLine = "Six Geese a Laying\n";     break;
            case 7:  day =  "seventh";   middleLine = "Seven Swans a Swimming\n"; break;
            case 8:  day =  "eighth";    middleLine = "Eight Maids a Milking\n";  break;
            case 9:  day =  "ninth";     middleLine = "Nine Ladies Dancing\n";    break;
            case 10: day =  "tenth";     middleLine = "Ten Lords a Leaping\n";    break;
            case 11: day =  "eleventh";  middleLine = "Eleven Pipers Piping\n";   break;
            case 12: day =  "twelfth";   middleLine = "12 Drummers Drumming\n";   break;
        }
        std::cout << "On the " << day << " day of Christmas\nmy true love sent to me:\n" << middleLine << lastLine <<
        std::endl;
        lastLine = middleLine + lastLine;
    }

    return 0;
}
