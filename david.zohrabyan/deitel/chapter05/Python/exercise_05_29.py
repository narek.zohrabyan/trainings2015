print 'Percent', 'Amount on deposit';
for rate in range(5, 11):
    print 'For', rate, '%';
    principal = 24.00;
    for year in range(1626, 2017):
        principal += principal * rate / 100;
        print 'In', year, 'year is', principal, '$';
