#include <iostream>
#include <cmath>

int
main()
{
    for (int i = 0; i <= 8; ++i) {
        int counter = std::abs(4 - i);
        for (int j = 0; j <= 8; ++j) {
            if (j >= counter && j <= 8 - counter) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
    }

    return 0;
}
