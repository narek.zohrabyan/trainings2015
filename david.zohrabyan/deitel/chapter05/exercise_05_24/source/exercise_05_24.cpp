#include <iostream>
#include <cmath>

int
main()
{   
    int n;
    std::cout << "Enter odd number (1-19): ";
    std::cin >> n;
    if (n < 1) {
        std::cerr << "Error 1: Entered number is less than 1." << std::endl;
        return 1;
    } else if (n > 19) {
        std::cerr << "Error 2: Entered number is bigger than 19." << std::endl;
    } else if (n % 2 == 0) {
        std::cerr << "Error 3: Entered number in not odd." << std::endl;
        return 3;
    }
    for (int i = 0; i < n; ++i) {
        int counter = std::abs(n / 2 - i);
        for (int j = 0; j < n; ++j) {
            if (j >= counter && j < n - counter) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
    }

    return 0;
}
