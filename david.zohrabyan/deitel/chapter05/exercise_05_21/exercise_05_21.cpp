#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << "This program considers weekly payment of employees.\n" << std::endl;
    int code = 0;
    while (code >= 0) {
        std::cout << "1-Manager\n"
                  << "2-Hourly worker\n"
                  << "3-Commision beggings worker\n"
                  << "4-Pieceworker\n" << std::endl;
 
        std::cout << "Employee type (negative to exit): " ;
        std::cin >> code;

        double weeklySalary;
        switch (code) {
        case 1: {
            std::cout << "Enter the fixed weekly salary: ";
            std::cin >> weeklySalary;
            if (weeklySalary < 0) {
                std::cerr << "Error 1: invalid weekly salary!." << std::endl;
                return 1;
            } 
            break;
        }
        case 2: {
            double hourlySalary;
            std::cout << "Enter salary for an hour: ";
            std::cin >> hourlySalary;
            if (hourlySalary < 0) {
                std::cerr << "Error 2: invalid hourly salary!." << std::endl;
                return 2;
            }
            int hours;
            std::cout << "Enter worked hours: ";
            std::cin >> hours;
            if (hours < 0) {
                std::cerr << "Error 3: invalid worked hours!." << std::endl;
                return 3;
            }
            weeklySalary = 40 * hourlySalary;
            if (hours > 40) {
                weeklySalary += (hours - 40) * (hourlySalary * 1.5);
            }     
            break;
        }
        case 3: {
            double weekSale;
            std::cout << "Enter the sum of week sales: ";
            std::cin >> weekSale;
            if (weekSale < 0) {
                std::cerr << "Error 4: invalid week sales!." << std::endl;
                return 4;
            } 
            weeklySalary = 250 + 0.057 * weekSale;    
            break;
        }
        case 4: {
            double fixedSalary;
            std::cout << "Enter the fixed salary from each developed commodity unit: ";
            std::cin >> fixedSalary;
            if (fixedSalary < 0) {
                std::cerr << "Error 5: invalid fixed salary!." << std::endl;
                return 5;
            }
            int quantity;
            std::cout << "Enter quantity of the developed goods for a week: ";
            std::cin >> quantity;
            if (quantity < 0) { 
                std::cerr << "Error 6. invalid quantity!." << std::endl;
                return 6;
            }
            weeklySalary = fixedSalary * quantity;
            break;
        }
        default:
            std::cerr << "Invalid code!. Try again." << std::endl;
            return 7;
        }
        std::cout << "Weekly Salary " << weeklySalary << std::endl;
        std::cout << std::endl;
    }
       
    return 0;
}
