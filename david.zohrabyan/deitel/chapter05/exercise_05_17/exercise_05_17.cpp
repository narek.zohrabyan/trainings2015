#include <iostream>

int
main()
{
    int i = 1, j = 2, k = 3, m = 2;
    
    /* below given it is necessary to put parentheses because before removing a stream,he can't do compiling in cout I can't give him do do something without parentheses,because equality check operations, logical and the relations, on a priority below than the operator of a conclusion.*/   
    std::cout << (i == 1) << std::endl;
    std::cout << (j == 3) << std::endl;
    std::cout << (i >= 1 && j <= 4) << std::endl;
    std::cout << (m <= 99 && k < m) << std::endl;
    std::cout << (j >= 1 || k == m) << std::endl;

    ///and here, given below external parentheses aren't required because during a conclusion the opertion of denial is carried out at once.!(operation) he can compiling, because because the operator of a conclusion displays result.
    std::cout << !m << std::endl;
    std::cout << !(j - m) << std::endl;
    std::cout << !(k > m) << std::endl;

    return 0;
}
