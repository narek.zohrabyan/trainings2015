#include <iostream>

int
main()
{
    std::cout << "Leg1\tLeg2\tHypotenuse" << std::endl;
    for (int leg1 = 1; leg1 <= 500; ++leg1) {
        int leg1sq = leg1 * leg1;
        for(int leg2 = leg1 + 1; leg2 <= 500; ++leg2) {
            int leg2sq = leg2 * leg2;
            for(int hypotenuse = leg2 + 1; hypotenuse <= 500; ++hypotenuse) {
                if(hypotenuse < leg1 + leg2) {
                    int hypsq = hypotenuse * hypotenuse;
                    if(hypsq == leg1sq + leg2sq) {
                        std::cout << leg1 << "\t" << leg2 << "\t" << hypotenuse << std::endl;
                        break;
                    }
                } 
            }   
        }
    }
    return 0;
}
