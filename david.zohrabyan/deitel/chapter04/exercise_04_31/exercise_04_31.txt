///the programmer wanted to perform at first operation (x+y),
 then to perform operation of a preincrement ++.

#include <iostream>

int
main()
{
    int x = 2, y = 3, z = x + y;
    while(z != 10) {
        std::cout << ++z << std::endl;
    }
    return 0;
}
