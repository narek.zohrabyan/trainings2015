#include <iostream>
 
int 
main()
{
    int number1;
 
    std::cout << "Enter  number: " << std::endl;
    std::cin >> number1;
 
    if (number1 % 2 == 0)
        std::cout << "Even" <<  std::endl;
 
    if (number1 % 2 != 0) 
        std::cout << "Not even" << std::endl;
  
    return 0;
}
