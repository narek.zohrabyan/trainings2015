#include <iostream>

int
main()
{
    for (int x = 19; x >= 1; x -= 2) {
        std::cout << x << std::endl;
    }
    return 0;
}
