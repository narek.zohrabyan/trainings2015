#include <iostream>

int
main()
{
    int value;
    std::cout << "Input your number: ";
    std::cin >> value;
    switch (value % 2) {
    case 0:
        std::cout << "\nEven integer" << std::endl;
        break;
    case 1:
        std::cout << "\nOdd integer" << std::endl;
        break;
    }
    return 0;
}
