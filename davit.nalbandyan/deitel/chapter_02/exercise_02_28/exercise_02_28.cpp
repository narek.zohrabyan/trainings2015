#include <iostream> 

int
main()
{
    int a, b, c, d, e, f;
    std::cout << "Enter five digit number: ";
    std::cin >> a;

    if(a > 9999){
        if(a < 100000){
            b = a / 10000;
            c = (a - (b * 10000)) / 1000;
            d = (a - (b * 10000) - (c * 1000)) / 100;
            e = (a - (b * 10000) - (c * 1000) - (d * 100)) / 10;
            f = (a - (b * 10000) - (c * 1000) - (d * 100) - (e * 10));
            std::cout << b << "   " << c << "   " << d << "   " << e << "   " << f << std::endl;
            return 0;
        }
    }
    std::cout << "Error\n";
    return 1;
}
