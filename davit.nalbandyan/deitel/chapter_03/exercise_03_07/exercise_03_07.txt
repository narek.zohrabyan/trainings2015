Обычно класс состоит из одной или нескольких элемент-функций, которые 
манипулируют атрибутами, принадлежащими конкретному объекту данного 
класса. Атрибуты представляются переменными в определении класса. Эти  
переменные называются элементами данных и объявляются внутри определения 
класса, но вне тела определений его элемент-функций. Каждый объект класса 
сохраняет в памяти свой собственный экземпляр своих атрибутов. 
