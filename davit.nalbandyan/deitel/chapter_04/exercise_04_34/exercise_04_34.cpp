#include <iostream>

int
main()
{
    int number;
    std::cout << "Input 4 digit number: ";
    std::cin >> number;
    if (number >= 10000) {
        std::cout << "\nInput not valid, closing the program" << std::endl;
        return 1;
    } else if (number <= 999) {
        std::cout << "\nInput not valid, closing the program" << std::endl;
        return 1;
    }
    int digit1 = number / 1000;
    int digit2 = (number / 100) % 10;
    int digit3 = (number / 10) % 10;
    int digit4 = number % 10;
    int encrypted_digit1 = (digit1 + 7) % 10;
    int encrypted_digit2 = (digit2 + 7) % 10;
    int encrypted_digit3 = (digit3 + 7) % 10;
    int encrypted_digit4 = (digit4 + 7) % 10;
    int encrypted_number = encrypted_digit3 * 1000 + encrypted_digit4 * 100 + encrypted_digit1 * 10 + encrypted_digit2;
    std::cout << "Your encrypted number is : " << encrypted_number << std::endl;
    return 0;
}
