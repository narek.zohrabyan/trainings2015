#include <iostream>

int
main()
{
    int encrypted_number;
    std::cout << "Input the encrypted number (4 digital): ";
    std::cin >> encrypted_number;
    if (encrypted_number >= 10000) {
        std::cout << "\nInput not valid, closing the program" << std::endl;
        return 1;
    } else if (encrypted_number <= 999) {
        std::cout << "\nInput not valid, closing the program" << std::endl;
        return 1;
    }
    int encrypted_digit1 = encrypted_number / 1000;
    int encrypted_digit2 = (encrypted_number / 100) % 10;
    int encrypted_digit3 = (encrypted_number / 10) % 10;
    int encrypted_digit4 = encrypted_number % 10;
    int digit1 = (encrypted_digit3 + 3) % 10;
    int digit2 = (encrypted_digit4 + 3) % 10;
    int digit3 = (encrypted_digit1 + 3) % 10;
    int digit4 = (encrypted_digit2 + 3) % 10;
    int number = digit1 * 1000 + digit2 * 100 + digit3 * 10 + digit4;
    std::cout << "\nYour decrypted number is : " << number << std::endl;
    return 0;
}
