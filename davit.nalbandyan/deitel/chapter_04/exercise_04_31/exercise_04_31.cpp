#include <iostream>

int
main()
{
    int x = 1;
    int y = 2;
    int z = x + y;
    std::cout << ++z << std::endl; 
    return 0;
}
//increment doesn't work in ++(x + y);
