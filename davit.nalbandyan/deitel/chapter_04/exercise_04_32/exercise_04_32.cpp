#include <iostream>

int
main()
{
    double side1, side2, side3;
    std::cout << "Input three sides of triangle (side1, side2, side3): ";
    std::cin >> side1 >> side2 >> side3;
    if (side1 + side2 < side3) {
        std::cout << "\nTriangle with these sides doesn't exist" << std::endl;
        return 1;
    } 
    if (side1 + side3 < side2) {
        std::cout << "\nTriangle with these sides doesn't exist" << std::endl;
        return 1;
    } 
    if (side3 + side2 < side1) {
        std::cout << "\nTriangle with these sides doesn't exist" << std::endl;
        return 1;
    } 
    std::cout << "Triangle with these sides exist" << std::endl;
    return 0;
}
