#include <iostream>

int 
main()
{
    int side1, side2, side3;
    std::cout << "Input sides of the right triangle (side1, side2, side3): ";
    std::cin >> side1 >> side2 >> side3;
    side1 *= side1;
    side2 *= side2;
    side3 *= side3;
    if ((side1 + side2) == side3) {
        std::cout << "\nThis right triangle exist." << std::endl;
    } else if ((side3 + side2) == side1) {
        std::cout << "\nThis right triangle exist." << std::endl;
    } else if ((side1 + side3) == side2) {
        std::cout << "\nThis right triangle exist." << std::endl;
    } else {
        std::cout << "\nThis right triangle doesn't exist." << std::endl;
    }
    return 0;
}
