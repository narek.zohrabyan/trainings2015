#include <iostream>

int
main()
{
    int number;
    std::cout << "Input your number: ";
    std::cin >> number;
    if (number < 0) {
        std::cout << "\nError1: Input not valid" << std::endl;
        return 1;
    } 
    int i = 2, factorial = 1;
    while (i <= number) {
        factorial *= i;
        ++i;
    }
    std::cout << "\nThe factorial of your number is: " << factorial << std::endl;
    return 0;
}
