#include<iostream>

int 
main()
{
    int a, b;

    std::cout << "Enter 2 numbers:" << std::endl;
    std::cin >> a >> b;

    if(a != 0){
        if (b % a == 0)
            std::cout << a << " is multiple of " << b << std::endl;
        if (b % a != 0)
            std::cout << a << " is not multiple of " << b << std::endl;
        return 0;
    }

    std::cout << "Error\n" << std::endl;
    return 1;
}
