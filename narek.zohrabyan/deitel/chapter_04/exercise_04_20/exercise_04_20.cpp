#include <iostream>

int
main()
{
    int studentCounter = 0, passes = 0, failure = 0;
    while (studentCounter < 10) {
        int result;
        std::cout << "Enter result (1 = pass, 2 = fail): ";
        std::cin >> result;
        if (1 == result) {
            passes += 1;
            ++studentCounter;
        } else if (2 == result) {
            failure += 1;
            ++studentCounter;
        } else {
            std::cout << "Please enter 1 for pass or 2 for fail. Try again!" << std::endl;
        }
    }
    std::cout << "\nPassed " << passes
              << "\nFailed " << failure << std::endl;
    if (passes > 8) {
        std::cout << "Raise tuition" << std::endl;
    }
    
    return 0;
}
