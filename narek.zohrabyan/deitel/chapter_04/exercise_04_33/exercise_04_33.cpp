#include <iostream>
 
int
main()
{
    double side1, side2, side3;
    std::cout << "Please import triangle sides: ";
    std::cin >> side1 >> side2 >> side3;
     if (side1 <= 0) {
         std::cout << "Error 1: Triangle side can not be zero or the negative. Try again!" << std::endl;
         return 1;
     }
     if (side2 <= 0) {
         std::cout << "Error 1: Triangle side can not be zero or the negative․ Try again!" << std::endl;
         return 1;
     }
     if (side3 <= 0) {
         std::cout << "Error 1: Triangle side can not be zero or the negative․ Try again!" << std::endl;
         return 1;
     }
     side1 *= side1;
     side2 *= side2;
     side3 *= side3;
     if (side2 + side3 == side1) {
         std::cout << "They can represent the rectangular sides!" << std::endl;
     } else if (side1 + side3 == side2) {
         std::cout << "They can represent the rectangular sides!" << std::endl;
     } else if (side1 + side2 == side3) {
         std::cout << "They can represent the rectangular sides!" << std::endl;
     } else {
         std::cout << "They can not represent the rectangular sides. Try again!" << std::endl;
     }
     return 0;
}
