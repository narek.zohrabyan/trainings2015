#include <iostream>

int
main()
{
    int length;
    std::cout << "Enter square length: ";
    std::cin >> length;
    if (length < 1) {
        std::cout << "Error 1: Invalid square length. Try again!" << std::endl;
        return 1;
    }
    if (length > 20) {
        std::cout << "Error 1: Invalid square length. Try again!" << std::endl;
        return 1;
    }
    int i = 0;
    while (i < length) {
        int j = 0;
        while (j < length) {
            if (0 == i) {
                std::cout << "*";
            } else if (length - 1 == i) {
                std::cout << "*";
            } else  if (0 == j) {
                std::cout << "*";
            } else if (length - 1 == j) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            ++j;
        }
        std::cout << std::endl;
        ++i;
    }          
    std::cout << std::endl;
    return 0;
}
