#include <iostream>

int
main()
{
    int goldenPI = 314159, counter = 1, sign = 1, count = 0;
    double pi = 0.0;
    for (int multiple = 100, divisor = 1000; divisor >= 1; divisor /= 10, multiple *= 10) {
        int currentPI = goldenPI / divisor;
        while (pi * multiple - currentPI >= 0.1 || pi * multiple - currentPI < 0) {
           pi += 4.0 / counter * sign;
           sign *= -1;
           counter += 2;
           ++count;
        } 
        std::cout << "PI: " << pi << "\tCount: " << count << std::endl;
    }    

    return 0;
}
