#include <iostream>

int
main()
{
    int code = 0;
    while (code != -1) {
        std::cout << "Enter code employee (-1 to exit): ";
        std::cin >> code;
        if (-1 == code) {
            return 1;
        }
        double salary = 0.0;
        switch (code) {
        case 1: {
            double fixSalary = 0.0;
            std::cout << "Enter manager fixed weekly salary: ";
            std::cin >> fixSalary;
            salary = fixSalary;
            break;
        }
        case 2: {
            double hourlySalary = 0.0;
            int time; 
            std::cout << "Enter hourly worker fixed hourly salary: ";
            std::cin >> hourlySalary;
            std::cout << "Enter working time: ";
            std::cin >> time;
            salary = hourlySalary * time;
            if (time > 40) {
                salary *= 1.5 - 20.0 / time;
            } 
            break;
        }
        case 3: {        
            double commissionSalary = 0.0;
            double sales = 0.0;
            std::cout << "Enter commission worker weekly sales: ";
            std::cin >> sales;
            commissionSalary = 250 + 5.7 * sales / 100;
            salary = commissionSalary;
            break;
        }
        case 4: {
            double pieceSalary = 0.0;
            int amount = 0;    
            std::cout << "Enter pieceworker fixed amount of money per item: "; 
            std::cin >> pieceSalary;
            std::cout << "Enter amount of per item: ";
            std::cin >> amount;
            salary = pieceSalary * amount;
            break;
        }
        default:
            std::cout << "Error 1: Incorrect code!" << std::endl;
            return 1;
        }
        std::cout << "Salary: " << salary << std::endl;
    }
    return 0;
} 
