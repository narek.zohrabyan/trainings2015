#include <iostream>

int
main()
{
    int counter = -1, number = 0;
    double sum = 0.0;
    do {
        sum += number;
        std::cout << "Enter the number: ";
        std::cin >> number;
        ++counter;
    } while (9999 != number);
    std::cout << "Average numbers = " << sum / counter << std::endl;
    return 0;
}
