#include <iostream>

int
main()
{  
    double total = 0.0;
    int number = 0;
    while (-1 != number) {
        std::cout << "Product number (-1 to exit): ";
        std::cin >> number;
        if (-1 == number) {
            break;
        } else if (number <= 0 || number > 5) {
            std::cerr << "Error 1: Invalid product number. Try again!" << std::endl;
            return 1;
        }
        int quantity;
        std::cout << "Quantity sold: ";
        std::cin >> quantity;
        if (quantity <= 0) {
            std::cerr << "Error 2: Invalid quantity. Try again!" << std::endl;
            return 2;
        }
        switch (number) {
        case 1: total += 2.98 * quantity; break;
        case 2: total += 4.50 * quantity; break;
        case 3: total += 9.98 * quantity; break;
        case 4: total += 4.49 * quantity; break;
        case 5: total += 6.87 * quantity; break;
        default: 
            std::cerr << "Error 1: Invalid product number. Try again!" << std::endl;
            return 1;
        }
    }
    std::cout << "The products sold: " << total << "$" << std::endl;
    return 0;
}
