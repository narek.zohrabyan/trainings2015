#include <iostream>

int 
main()
{
    std::cout << "Side1" << "\tSide2" << "\tHypotenuse" << std::endl;
    for (int side1 = 3; side1 <= 500; ++side1) {
        int side1square = side1 * side1;
        for (int side2 = side1 + 1; side2 <= 500; ++side2) {
            int side2square = side2 * side2;
            for (int hypotenuse = side2 + 1; hypotenuse <= 500; ++hypotenuse) {
                if (hypotenuse < side1 + side2) {
                    int hypotenuseSquare = hypotenuse * hypotenuse;
                    if (hypotenuseSquare == side1square + side2square) {
                            std::cout << side1 << "\t" << side2 << "\t" << hypotenuse << std::endl;
                            break;
                    }
                }
            }
        }
    }
    return 0;
}
