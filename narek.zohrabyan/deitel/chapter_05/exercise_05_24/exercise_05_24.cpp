#include <iostream>
#include <cmath>

int
main()
{
    int number;
    std::cout << "Enter odd number (1 - 19): ";
    std::cin >> number;
    if (number < 1 || number > 19) {
        std::cout << "Error 1: Invalid number!" << std::endl;
        return 1;
    }
    if (number % 2 == 0) {
        std::cout << "Error 2: Enter odd number!" << std::endl;
        return 2;
    }
    int condition = number / 2;
    for (int i = -condition; i <= condition; ++i) {
        int absolute = std::abs(i);
        for (int j = -condition; j <= condition; ++j) {
            std::cout << (absolute + std::abs(j) <= condition ? "*" : " ");
        }
        std::cout << std::endl;
    }
    return 0;
}
