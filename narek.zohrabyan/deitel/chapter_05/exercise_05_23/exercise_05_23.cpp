#include <iostream>
#include <cmath>

int
main() 
{ 
    for (int i = -4; i <= 4; ++i) {
        int absolute = std::abs(i);
        for (int j = -4; j <= 4; ++j) {
            std::cout << (absolute + std::abs(j) <= 4 ? "*" : " ");
        }
        std::cout << std::endl;
    }        
    return 0;
}
