#include <iostream>
#include <iomanip>

int
main()
{
    double principal = 1000.0;
    for (int rate = 5; rate <= 10; ++rate) {
        std::cout << std::fixed << std::setprecision(2);
        std::cout << "Rate = " << rate << "%" << std::endl;
        std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;
        double degree = 1.0 + rate * 0.01, amount = principal;
        for (int year = 1; year <= 10; ++year) { 
            amount *= degree;
            std::cout << std::setprecision(2) << year << std::setw(21) << amount << std::endl;
        }
    }
    return 0;
}       
