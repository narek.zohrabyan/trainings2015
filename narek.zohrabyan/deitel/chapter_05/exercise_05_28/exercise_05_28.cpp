#include <iostream>

int
main()
{
    std::string couplet;
    std::cout << "The Twelve days of Christmas\n" << std::endl;
    for (int n = 1; n <= 12; ++n) {
        std::string day, line;
        switch (n) {
        case 1:  day = "first";    line = "A Partridge in a Pear Tree\n"; break;
        case 2:  day = "second";   line = "Two Turtle Doves\nand ";       break;
        case 3:  day = "third";    line = "Three French Hens\n";          break;
        case 4:  day = "fourth";   line = "Four Calling Birds*\n";        break;
        case 5:  day = "fifth";    line = "Five Golden Rings\n";          break;
        case 6:  day = "sixth";    line = "Six Geese a Laying\n";         break;
        case 7:  day = "seventh";  line = "Seven Swans a Swimming\n";     break;
        case 8:  day = "eight";    line = "Eight Maids a Milking\n";      break;
        case 9:  day = "ninth";    line = "Nine Ladies Dancing\n";        break;
        case 10: day = "tenth";    line = "Ten Lords a Leaping\n";        break;
        case 11: day = "eleventh"; line = "Eleven Pipers Piping\n";       break;
        case 12: day = "twelfth";  line = "12 Drummers Drumming\n";       break;
        }
        std::cout << "\nOn the " << day << " day of Christmas\nmy true love sent to me:\n" << line << couplet << std::endl;
        couplet = line + couplet;
    }
    return 0;
}
