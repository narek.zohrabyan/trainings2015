#include <iostream>
#include <iomanip>

int
main()
{
    for (int rate = 5; rate <= 10; ++rate) {
        std::cout << std::fixed << std::setprecision(2);
        std::cout << "Rate = " << rate << "%" << std::endl;
        std::cout << "Year" << std::setw(30) << "Amount on deposit" << std::endl;
        double principal = 24.00;
        for (int year = 1626; year <= 2016; ++year) { 
            principal += principal * rate / 100;
            std::cout << std::setprecision(2) << year << std::setw(30) << principal <<" $" << std::endl;
        }
    }
    return 0;
}       
