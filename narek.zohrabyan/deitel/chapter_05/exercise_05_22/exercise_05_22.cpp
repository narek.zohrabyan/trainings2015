#include <iostream>

int
main()
{
    int x, y;
    std::cout << "Enter x and y: ";
    std::cin >> x >> y;
    if ((!(x < 5) && !(y >= 7)) == !((x < 5) || (y >= 7))) {
        std::cout << "((!(x < 5) && !(y >= 7) and !((x < 5) || (y >= 7))) are equivalent." << std::endl;
    } else {
        std::cout << "((!(x < 5) && !(y >= 7) and (!(x < 5) || !(y >= 7))) are not equivalent." << std::endl;
    }
    int a, b, g;
    std::cout << "Enter a, b and g: ";
    std::cin >> a >> b >> g;
    if ((!(a == b) || !(g != 5)) == (!((a == b) && (g != 5)))) {
        std::cout << "((!(a == b) || !(g != 5)) and (!(a != b) && !(g == 5))) are equivalent." << std::endl;
    } else {
        std::cout << "((!(a == b) || !(g != 5)) and (!((a == b) && !(g != 5)))) are not equivalent." << std::endl;
    }
    std::cout << "Enter x and y: ";
    std::cin >> x >> y;
    if ((!((x <= 8) && (y > 4))) == ((x > 8) || (y <= 4))) {
        std::cout << "((!((x <= 8) && (y > 4))) and ((x > 8) || (y <= 4))) are equivalent." << std::endl;
    } else {
        std::cout << "((!((x <= 8) && (y > 4))) and ((x <= 8) || (y > 4))) are not equivalent." << std::endl;
    } 
    int i, j;
    std::cout << "Enter i and j: ";
    std::cin >> i >> j;
    if ((!((i > 4) || (j <= 6))) == ((i <= 4) && (j > 6))) {
        std::cout << "((!((i > 4) || (j <= 6))) and ((i > 4) && (j <= 6))) are equivalent." << std::endl;
    } else {
        std::cout << "((!((i > 4) || (j <= 6)) and ((i > 4) && (j <= 6))) are not equivalent." << std::endl;
    }
    return 0;
}
