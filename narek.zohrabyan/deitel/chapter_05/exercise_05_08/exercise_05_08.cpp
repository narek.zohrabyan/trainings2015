#include <iostream>

int
main()
{
    int quantity, min = 2147483647;
    std::cout << "Enter quantity: ";
    std::cin >> quantity;
    if (quantity <= 0 ) {
        std::cout << "Error 1: Entered quantity is invalid. Please try again!" << std::endl;
        return 1;
    } 
    for (int counter = 0; counter < quantity; ++counter) {
        int number;
        std::cout << "Enter the number: ";
        std::cin >> number;
        if (number < min ) {
            min = number;
        }  
    }
    std::cout << "Minimum number is  " << min << std::endl;
    return 0;
}
