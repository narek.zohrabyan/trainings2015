#include "GradeBook.hpp"
#include <iostream>

GradeBook::GradeBook(std::string name, std::string teacher)
{
    setCourseName(name);
    setTeacherName(teacher);
}

void 
GradeBook::setCourseName(std::string name)
{
    courseName_ = name;
}

std::string GradeBook::getCourseName()
{
    return courseName_;
}

void 
GradeBook::setTeacherName(std::string teacher)
{
    teacherName_ = teacher;
}

std::string GradeBook::getTeacherName()
{
    return teacherName_;
}

void 
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for: " << getCourseName() <<"!" << std::endl;
    std::cout << "This cource is presented by: " << getTeacherName() << "!" << std::endl;
}
