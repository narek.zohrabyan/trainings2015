#include "Invoice.hpp"
#include <iostream>

int 
main()
{
    Invoice invoice("ARIEL", "color 5kg", 10, 2500);

    std::cout << "Article: " << invoice.getArticle() 
              << "\nDescription: " << invoice.getDescription() 
              << "\nUnit: " << invoice.getQuantity() 
              << "\nPrice: " << invoice.getPrice() 
              << "\nInvoice Amount: " << invoice.getInvoiceAmount() << std::endl;

    return 0;
}
